package kz.kmf.test.testapp.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class RegistrationDataRequest(
    @SerializedName("regionId")
    @Expose
    val regionId: String,
    @SerializedName("cityId")
    @Expose
    val cityId: String,
    @SerializedName("educationId")
    @Expose
    val educationId: String,
    @SerializedName("doctypeId")
    @Expose
    val doctypeId: String,
    @SerializedName("comment")
    @Expose
    val comment: String
)