package kz.kmf.test.testapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kz.kmf.test.testapp.R
import kz.kmf.test.testapp.model.Place

class PlaceAdapter(regions: ArrayList<Place>, val type: Type) : BaseAdapter() {
    private val items = regions
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val id = when (type) {
            Type.REGION -> R.layout.region_item
            Type.CITY -> R.layout.city_item
        }
        return convertView?.apply {
            findViewById<TextView>(R.id.name).text = items[position].name
        }
            ?: LayoutInflater.from(parent?.context).inflate(id, parent, false)
                .apply {
                    findViewById<TextView>(R.id.name).text = items[position].name
                }
    }

    override fun getItem(position: Int): Place {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

    enum class Type {
        REGION,
        CITY
    }

}