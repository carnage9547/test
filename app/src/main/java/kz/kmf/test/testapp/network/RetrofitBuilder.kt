package kz.kmf.test.testapp.network

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitBuilder {
    private fun buildRetrofit(): Retrofit =
        Retrofit.Builder()
            .baseUrl("https://api.kmf.kz:8443/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

    fun getApi(): PlacesAPI =
        buildRetrofit().create(PlacesAPI::class.java)
}