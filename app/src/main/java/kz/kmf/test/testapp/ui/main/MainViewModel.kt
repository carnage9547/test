package kz.kmf.test.testapp.ui.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import kz.kmf.test.testapp.domain.BookReferenceRepository
import kz.kmf.test.testapp.domain.Result
import kz.kmf.test.testapp.domain.State
import kz.kmf.test.testapp.model.Place
import kz.kmf.test.testapp.model.ReferenceBookEntry

class MainViewModel : ViewModel() {

    val repository: BookReferenceRepository = BookReferenceRepository()

    private var region: MutableLiveData<Place> = MutableLiveData()
    private var place: MutableLiveData<Place> = MutableLiveData()

    val places: MutableLiveData<List<Place>> = MutableLiveData()
    val educations: MutableLiveData<List<ReferenceBookEntry>> = MutableLiveData()
    val doctypes: MutableLiveData<List<ReferenceBookEntry>> = MutableLiveData()
    init {
        repository.getPlaces().observeForever(object : Observer<Result<List<Place>?>> {
            override fun onChanged(it: Result<List<Place>?>?) {
                when (it?.state) {
                    State.LOADED -> {
                        places.value = it.data
                        repository.getPlaces().removeObserver(this)
                    }
                    State.EXCEPTION -> {
                        repository.getPlaces().removeObserver(this)
                    }
                    State.STARTED -> {

                    }
                }
            }

        })

        repository.getAll().observeForever(object : Observer<Result<List<ReferenceBookEntry>?>?> {
            override fun onChanged(it: Result<List<ReferenceBookEntry>?>?) {
                when (it?.state) {
                    State.LOADED -> {
                        educations.value = it.data?.filter { referenceBookEntry -> referenceBookEntry.type == "E_EDUCATION" }
                        doctypes.value = it.data?.filter { referenceBookEntry -> referenceBookEntry.type == "E_DOC_TYPE" }
                        repository.getAll().removeObserver(this)
                    }
                    State.EXCEPTION -> {
                        repository.getAll().removeObserver(this)
                    }
                    State.STARTED -> {

                    }
                }
            }

        })

    }

    fun setRegion(region: Place) {
        this.region.postValue(region)
    }

    fun getRegion(): LiveData<Place> {
        return region
    }

    fun setPlace(place: Place) {
        if (region.value?.id?.toLong() == place.getParentId()) {
            this.place.postValue(place)
        } else {
            throw IllegalArgumentException("Set valid region first")
        }
    }

    fun getPlace(): LiveData<Place> {
        return place
    }

    fun sendData() {
        
    }

}