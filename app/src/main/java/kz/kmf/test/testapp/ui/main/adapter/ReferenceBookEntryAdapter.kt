package kz.kmf.test.testapp.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import kz.kmf.test.testapp.R
import kz.kmf.test.testapp.model.ReferenceBookEntry

class ReferenceBookEntryAdapter(private val items: ArrayList<ReferenceBookEntry>) : BaseAdapter() {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return convertView?.apply {
            findViewById<TextView>(R.id.name).text = items[position].name
        }
            ?: LayoutInflater.from(parent?.context).inflate(
                R.layout.region_item, parent, false)
                .apply {
                    findViewById<TextView>(R.id.name).text = items[position].name
                }
    }

    override fun getItem(position: Int): Any {
        return items[position]
    }

    override fun getItemId(position: Int): Long {
        return items[position].id.toLong()
    }

    override fun getCount(): Int {
        return items.size
    }

}