package kz.kmf.test.testapp.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kz.kmf.test.testapp.model.ReferenceBookEntry

data class AllResponse(
    @SerializedName("success")
    @Expose
    val success: Boolean,
    @SerializedName("data")
    @Expose
    val data: List<ReferenceBookEntry>
)
