package kz.kmf.test.testapp.ui.main

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import com.google.gson.GsonBuilder
import kotlinx.android.synthetic.main.main_fragment.*
import kz.kmf.test.testapp.R
import kz.kmf.test.testapp.model.Place
import kz.kmf.test.testapp.model.ReferenceBookEntry
import kz.kmf.test.testapp.network.model.RegistrationDataRequest
import kz.kmf.test.testapp.ui.main.adapter.PlaceAdapter
import kz.kmf.test.testapp.ui.main.adapter.ReferenceBookEntryAdapter

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.main_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        viewModel.places.observe(this, ::updatePlaces)
        viewModel.getRegion().observe(this, ::setRegion)

        viewModel.educations.observe(this, ::updateEducations)
        viewModel.doctypes.observe(this, ::updateDocTypes)

        initCompleteListener();

    }

    var isDone: MutableLiveData<Boolean> = MutableLiveData()

    private fun initCompleteListener() {
        comment.addTextChangedListener {
            if (it?.length!! > 0) {
                checkDone()
            }
        }
        val changesel = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                checkDone()
            }

        }
        education.onItemSelectedListener = changesel
        doctypes.onItemSelectedListener = changesel
        button.setOnClickListener {
            if (it.isEnabled) {
                AlertDialog.Builder(this.context)
                    .setTitle("Готово")
                    .setMessage(
                        GsonBuilder().create().toJson(
                            RegistrationDataRequest(
                                region.selectedItemId.toString(),
                                city.selectedItemId.toString(),
                                education.selectedItemId.toString(),
                                doctypes.selectedItemId.toString(),
                                comment.text.toString()
                            )
                        )
                    )
                    .create().show()
            }
        }
    }

    private fun checkDone() {
        button.isEnabled = comment.text.isNotEmpty() &&
                region.selectedItem != null &&
                city.selectedItem != null &&
                education.selectedItem != null &&
                doctypes.selectedItem != null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        region.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                viewModel.setRegion(viewModel.places.value?.first { it.id.toLong() == id }!!)
            }
        }

    }

    fun setRegion(it: Place) {
//        region.setSelection((region.adapter as PlaceAdapter).items.indexOf(it))
        city.adapter =
            PlaceAdapter(
                ArrayList(viewModel.places.value!!.filter { place -> place.getParentId()?.toLong() == it.id }),
                PlaceAdapter.Type.CITY
            )
    }

    fun setCity(it: Place) {
//        city.setSelection((city.adapter as PlaceAdapter).items.indexOf(it))
    }

    fun updatePlaces(it: List<Place>) {
        region.adapter = PlaceAdapter(
            ArrayList(it.filter { place -> place.getParentId() == null }),
            PlaceAdapter.Type.REGION
        )
        city.adapter = PlaceAdapter(
            ArrayList(),
            PlaceAdapter.Type.CITY
        )
    }

    fun updateEducations(it: List<ReferenceBookEntry>) {
        education.adapter =
            ReferenceBookEntryAdapter(
                ArrayList(it)
            )
    }

    fun updateDocTypes(it: List<ReferenceBookEntry>) {
        doctypes.adapter =
            ReferenceBookEntryAdapter(
                ArrayList(it)
            )
    }


}
