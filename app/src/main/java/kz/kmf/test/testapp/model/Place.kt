package kz.kmf.test.testapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Place(

    @SerializedName("id")
    @Expose
    val id: Long,

    @SerializedName("name")
    @Expose
    val name: String,

    @SerializedName("parentId")
    @Expose
    val parent_id: String
) {
    fun getParentId(): Long? {
        return try {
            Integer.parseInt(parent_id).toLong()
        } catch (e: Exception) {
            null
        }
    }
}