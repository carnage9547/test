package kz.kmf.test.testapp.network.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PlacesRequest(
    @SerializedName("type")
    @Expose
    val type: String
)
