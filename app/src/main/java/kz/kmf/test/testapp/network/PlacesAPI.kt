package kz.kmf.test.testapp.network

import kz.kmf.test.testapp.network.model.AllResponse
import kz.kmf.test.testapp.network.model.PlacesRequest
import kz.kmf.test.testapp.network.model.PlacesResponse
import retrofit2.http.Body
import retrofit2.http.POST

interface PlacesAPI {
    @POST("cabinet/reference_book")
    suspend fun getPlaces(@Body requestBody: PlacesRequest): PlacesResponse
    @POST("cabinet/reference_book")
    suspend fun getAll(@Body requestBody: PlacesRequest): AllResponse
}