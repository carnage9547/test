package kz.kmf.test.testapp.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


data class ReferenceBookEntry(
    @SerializedName("id")
    @Expose
    val id: Int,
    @SerializedName("name")
    @Expose
    val name: String,
    @SerializedName("arh")
    @Expose
    val arh: Int,
    @SerializedName("type")
    @Expose
    val type: String,
    @SerializedName("orderPos")
    @Expose
    val orderPos: Int
)