package kz.kmf.test.testapp.domain

import androidx.lifecycle.LiveData
import androidx.lifecycle.liveData
import kz.kmf.test.testapp.model.Place
import kz.kmf.test.testapp.model.ReferenceBookEntry
import kz.kmf.test.testapp.network.ApiCaller
import kz.kmf.test.testapp.network.model.PlacesRequest

class BookReferenceRepository {
    private var cachedPlaces: Result<List<Place>?>? = null
    private var cachedAll: Result<List<ReferenceBookEntry>?>? = null
    fun getPlaces(): LiveData<Result<List<Place>?>> {
        return liveData {
            if (cachedPlaces != null) {
                emit(cachedPlaces!!)
            } else {
                emit(Result<List<Place>?>(State.STARTED, null))
                val result = ApiCaller.getPlaces(PlacesRequest("R_ADDR"))
                if (result.success) {
                    emit(Result<List<Place>?>(State.LOADED, result.data))
                    cachedPlaces = this.latestValue
                } else {
                    emit(Result<List<Place>?>(State.EXCEPTION, emptyList()))
                }
            }
        }
    }

    fun getAll(): LiveData<Result<List<ReferenceBookEntry>?>> {
        return liveData {
//            emit(Result<List<ReferenceBookEntry>?>(State.STARTED, null))
            if (cachedAll != null) {
                emit(cachedAll!!)
            } else {
                val result = ApiCaller.getAll(PlacesRequest("E_ALL"))
                if (result.success) {
                    emit(Result<List<ReferenceBookEntry>?>(State.LOADED, result.data))
                } else {
                    emit(Result<List<ReferenceBookEntry>?>(State.EXCEPTION, emptyList()))
                }
                cachedAll = this.latestValue
            }
        }
    }
}

data class Result<T>(
    val state: State,
    val data: T
)

enum class State {
    STARTED,
    LOADED,
    EXCEPTION
}